﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPHub : MonoBehaviour
{
    public Vector3 respawnHub;
    // Start is called before the first frame update
    void Start()
    {
        respawnHub = new Vector3(78, 1, 28);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        { 
            other.gameObject.transform.position = respawnHub + new Vector3(1.5f, 0, 0);
            other.gameObject.transform.rotation = Quaternion.identity;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Respawn.respawnPoint = respawnHub;
            }
            other.gameObject.transform.position = respawnHub;
            other.gameObject.transform.rotation = Quaternion.identity;
        }
    }
}
