﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AffichageBallons : MonoBehaviour
{
    private int ballonsBoom = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void PrintBallons()
    {
        ballonsBoom++;
        gameObject.GetComponent<Text>().text = "Ballons éclatés : " + ballonsBoom + "/6";
        if(ballonsBoom == 6)
        {
            GameManager.Instance.InstantiateCat();
        }
    }

}
