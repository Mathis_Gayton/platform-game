﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerGoal : MonoBehaviour
{

    public Text text;
    private bool isPlayed = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Ball"))
        {
            text.gameObject.SetActive(true);
            if(isPlayed == false)
            {
                gameObject.GetComponent<AudioSource>().Play();
                isPlayed = true;
                GameManager.Instance.InstantiateCat();
            }
            StartCoroutine("Goal"); 
        }
    }

    private IEnumerator Goal()
    {
        while(text.color.a != 0)
        {
            Color col = text.color;
            col.a -= 0.05f;
            text.color = col;
            yield return new WaitForSeconds(0.1f);
        }

        yield return null;
    }
}
