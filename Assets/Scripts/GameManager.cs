﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    public Canvas canvUI;
    public Canvas canvMenu;
    public GameObject cat;

    public static int instanceCat = 0;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if(instance == null)
            {
                Debug.LogError("GameManager non initialisé");
            }

            return instance;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("On affiche le Menu");
            if (canvMenu.enabled)
            {
                canvMenu.enabled = false;
            }
            else
            {
                canvMenu.enabled = true;
            }
            Movements.isMenu = canvMenu.enabled;
        }

        if(instanceCat == 3)
        {
            Debug.Log("Victoire");
        }
    }


    public void Reticule(bool active)
    {
        for (int i= 0; i < canvUI.transform.childCount; i++)
        {
            if(canvUI.transform.GetChild(i).gameObject.name == "Reticule")
            {
                canvUI.transform.GetChild(i).gameObject.SetActive(active);
            }
        }
    }

    public void InstantiateCat()
    {
        Instantiate(cat, Player.transform.position, Player.transform.rotation);
        instanceCat++;
    }
}
