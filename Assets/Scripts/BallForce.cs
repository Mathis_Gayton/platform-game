﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallForce : MonoBehaviour
{
    private Vector3 lastPos;
    private Transform trans;
    private Rigidbody rb;

    public Transform playerTransform;


    // Start is called before the first frame update
    void Start()
    {
        trans = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        lastPos = trans.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            if(Vector3.Distance(gameObject.transform.position, playerTransform.position) < 1.9)
            {
                Debug.Log("On est assez proche");
                Vector3 dir = (gameObject.transform.position - playerTransform.position).normalized;
                rb.velocity = Vector3.zero;
                rb.AddForce((dir + Vector3.up * 2) * 50);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player") && collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 0.0008)
        {
            Debug.Log(collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude);
            Vector3 dir = collision.contacts[0].point - trans.position;
            dir = -dir.normalized;
            rb.AddForce(dir * 50);
        }

    }


}
