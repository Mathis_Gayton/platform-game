﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeBalloons : MonoBehaviour
{
    public GameObject textBalloons;
    private bool isTriggered = false;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(isTriggered == false)
        {
            textBalloons.SendMessage("PrintBallons");
            gameObject.GetComponent<AudioSource>().Play();
        }
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        isTriggered = true;
        Destroy(gameObject,4f);
    }
}
