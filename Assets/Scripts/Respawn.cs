﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    public static Vector3 respawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        respawnPoint = new Vector3(78,1,26);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.position = respawnPoint;
    }
}
