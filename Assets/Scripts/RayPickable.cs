﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayPickable : MonoBehaviour
{

    public GameObject makarovHolder;
    public GameObject dynamiteHolder;
    public Transform Table;

    private Vector3 objectSpawn;
    private Vector3 DynamiteSpawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, 2.0f))
            {
                Debug.Log("On a touche quelque chose ! " + hit.collider.gameObject.name);
                if(hit.collider.gameObject.CompareTag("Pickable"))
                {
                    if(hit.collider.name == "Dynamite")
                    {
                        Transform pos = hit.collider.GetComponent<Transform>();
                        DynamiteSpawn = pos.position;
                        pos.SetParent(dynamiteHolder.GetComponent<Transform>());
                        pos.position = dynamiteHolder.GetComponent<Transform>().position - new Vector3(0, 0.1f, 0);
                        pos.localRotation = Quaternion.Euler(0, 0, 0);
                        hit.collider.GetComponent<CapsuleCollider>().enabled = false;
                        GameManager.Instance.Reticule(true);
                    }
                    else
                    {
                        Transform pos = hit.collider.GetComponent<Transform>();
                        objectSpawn = pos.position;
                        pos.SetParent(makarovHolder.GetComponent<Transform>());
                        pos.position = makarovHolder.GetComponent<Transform>().position - new Vector3(0, 0.1f, 0);
                        pos.localRotation = Quaternion.Euler(0, 20, 0);
                        hit.collider.GetComponent<MeshCollider>().enabled = false;
                        GameManager.Instance.Reticule(true);
                    }
                }
            }
        }

        if(Input.GetMouseButtonDown(1))
        {
            if(makarovHolder.GetComponent<Transform>().childCount != 0)
            {
                Transform child = makarovHolder.GetComponent<Transform>().GetChild(0);
                child.transform.position = objectSpawn;
                child.GetComponent<MeshCollider>().enabled = true;
                child.transform.SetParent(Table);
                GameManager.Instance.Reticule(false);
            }
            else if(dynamiteHolder.GetComponent<Transform>().childCount != 0)
            {
                Transform child = dynamiteHolder.GetComponent<Transform>().GetChild(0);
                child.transform.position = DynamiteSpawn;
                child.GetComponent<CapsuleCollider>().enabled = true;
                child.transform.SetParent(null);
                GameManager.Instance.Reticule(false);
            }
        }
    }
}
