﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporationLevel1 : MonoBehaviour
{
    private Vector3 level1;

    // Start is called before the first frame update
    void Start()
    {
        level1 = new Vector3(79.2f, 1f, 267.6f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            other.gameObject.transform.position = level1 + new Vector3(1.5f,0,0);
            other.gameObject.transform.rotation = Quaternion.identity;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Respawn.respawnPoint = level1;
            }
            other.gameObject.transform.position = level1;
            other.gameObject.transform.rotation = Quaternion.identity;
        }

    }
}
