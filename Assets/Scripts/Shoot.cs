﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform camTransform;
    public Transform playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        gameObject.transform.rotation = camTransform.rotation;

            if (Input.GetMouseButtonDown(0))
        {
            if(gameObject.transform.parent.gameObject.name == "Makarov Holder")
            {
                GameObject bullet = Instantiate(bulletPrefab, gameObject.transform.parent.position, Quaternion.Euler(0,0,90));
                Rigidbody rb = bullet.GetComponent<Rigidbody>();
                rb.useGravity = false;  
                rb.velocity = transform.TransformDirection(Vector3.forward * 40);
                Destroy(bullet, 7f);
            }
        }
    }
}
