﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movements : MonoBehaviour
{

    private Transform position;
    public Camera cam;
    private Transform camTransform;

    public float movementSpeed = 9.0f;
    private float mouseX, mouseY;

    private bool jump;
    public static bool isMenu;

    // Start is called before the first frame update
    void Start()
    {
        position = gameObject.GetComponent<Transform>();
        camTransform = cam.GetComponent<Transform>();
        jump = true;
        isMenu = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isMenu == false)
        {
            position.Translate(Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime, 0, Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime);

            mouseX = Input.GetAxis("Mouse X");
            mouseY = Input.GetAxis("Mouse Y");

            mouseY = Mathf.Clamp(mouseY, -90, 90);
            camTransform.Rotate(-mouseY, 0, 0);
            position.Rotate(0, mouseX, 0);
        }
    }

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(jump)
            {
                jump = false;
                GetComponent<Rigidbody>().AddForce(Vector3.up * 3000);
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        jump = true;
    }
}
