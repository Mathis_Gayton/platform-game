﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowDynamite : MonoBehaviour
{
    public Transform camTransform;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        gameObject.transform.rotation = camTransform.rotation;

        if (gameObject.transform.parent != null)
        {
            if (Input.GetMouseButton(0))
            {
                Debug.Log("On lance !");
                Rigidbody rb = gameObject.AddComponent<Rigidbody>();
                rb.useGravity = true;
                rb.velocity = transform.TransformDirection(Vector3.forward * 20);
                gameObject.transform.parent = null;
                Destroy(gameObject, 7f);
                GameManager.Instance.Reticule(false);
            }
        }
    }
}
