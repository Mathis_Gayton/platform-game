﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public GameObject Menu;
    public GameObject Options;
    public Canvas MenuCanvas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackGame()
    {
        MenuCanvas.enabled = false;
        Movements.isMenu = false;
    }

    public void BackMenu()
    {
        Options.gameObject.SetActive(false);
        Menu.gameObject.SetActive(true);
    }

    public void OptionMenu()
    {
        Options.gameObject.SetActive(true);
        Menu.gameObject.SetActive(false);
    }
}
