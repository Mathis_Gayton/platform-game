﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volume : MonoBehaviour
{
    Slider slider;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
         slider = gameObject.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        audio.volume = slider.value;
    }
}
