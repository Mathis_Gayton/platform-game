﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour
{

    private GameObject Player;
    private NavMeshAgent navagent;
    private Vector3 offsetSpawn = new Vector3(2f,0f,1f);
    private Vector3 offsetFollow = new Vector3(2f, 0f, 0f);

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        gameObject.GetComponent<Transform>().position = Player.transform.position + offsetSpawn;
        navagent = gameObject.AddComponent<NavMeshAgent>();
        navagent.speed = 7;
        navagent.stoppingDistance = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(Player.transform.position.magnitude - gameObject.transform.position.magnitude) > 20)
        {
            navagent.Warp(Player.transform.position);
        }
        navagent.SetDestination(Player.transform.position + offsetFollow);
    }

}
